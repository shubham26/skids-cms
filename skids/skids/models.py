from django.db import models
from cms.models import CMSPlugin


class ImageModel(CMSPlugin):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='media', null=True)

    class Meta:
        abstract = True


class GalleryPluginModel(ImageModel):

    def __str__(self):
        return self.name


class DegreeModel(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class StaffPluginModel(ImageModel):
    desigination = models.CharField(max_length=100)
    degree = models.ManyToManyField('skids.DegreeModel')

    def __str__(self):
        return self.name


class FacilityPluginModel(ImageModel):
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name

