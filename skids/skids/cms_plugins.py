from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from django.utils.translation import ugettext as _

from skids import models


@plugin_pool.register_plugin  # register the plugin
class GalleryPluginPublisher(CMSPluginBase):
    model = models.GalleryPluginModel  # model where plugin data are saved
    module = _("Gallery")
    name = _("Gallery Plugin")  # name of the plugin in the interface
    render_template = "plugins/gallery.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin  # register the plugin
class IndexGalleryPluginPublisher(CMSPluginBase):
    model = models.GalleryPluginModel  # model where plugin data are saved
    module = _("Gallery")
    name = _("Index Gallery Plugin")  # name of the plugin in the interface
    render_template = "plugins/index-gallery-section.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin  # register the plugin
class IndexStaffPluginPublisher(CMSPluginBase):
    model = models.StaffPluginModel  # model where plugin data are saved
    module = _("Staff")
    name = _("Index Staff Plugin")  # name of the plugin in the interface
    render_template = "plugins/index-staff.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context


@plugin_pool.register_plugin  # register the plugin
class IndexFacilityPluginPublisher(CMSPluginBase):
    model = models.FacilityPluginModel  # model where plugin data are saved
    module = _("Facility")
    name = _("Index Facility Plugin")  # name of the plugin in the interface
    render_template = "plugins/index-facility.html"

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context
